<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrometsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nacinplacanja_id');
            $table->string('sektor_id');
            $table->double('ulaz_izlaz');
            $table->string('napomena')->nullable();
            $table->float('iznos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promets');
    }
}
