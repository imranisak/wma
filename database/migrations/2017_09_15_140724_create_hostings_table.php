<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hostings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('naziv_domene');
            $table->string('domena_id');
            $table->string('server_id');
            $table->string('kontakt')->nullable();
            $table->string('mail')->nullable();
            $table->string('telefon')->nullable();
            $table->string('datum_obnove');
            $table->string('paket_id');
            $table->float('cijena')->nullable();
            $table->string('napomena')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hostings');
    }
}
