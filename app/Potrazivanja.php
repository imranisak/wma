<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Potrazivanja extends Model
{
    protected $fillable = ['iznos'];
}
