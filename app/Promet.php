<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promet extends Model
{
    protected $fillable = ['nacinplacanja_id','sektor_id','iznos','napomena','ulaz_izlaz','iznos'];
    
    public function sektor(){
    	return $this->belongsTo(Sektor::class);
    }   
    	public function nacinplacanja(){
    		return $this->belongsTo(NacinPlacanja::class);
    }
}
