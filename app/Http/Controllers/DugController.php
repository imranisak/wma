<?php

namespace App\Http\Controllers;

use App\Dug;
use Illuminate\Http\Request;

class DugController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Dug::create($request->all());
        session()->flash('success','Snimljeno');
        return redirect ('/stanje');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dug  $dug
     * @return \Illuminate\Http\Response
     */
    public function show(Dug $dug)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dug  $dug
     * @return \Illuminate\Http\Response
     */
    public function edit(Dug $dug)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dug  $dug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dug $dug)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dug  $dug
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dug $dug)
    {
        //
    }
}
