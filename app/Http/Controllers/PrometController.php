<?php

namespace App\Http\Controllers;

use App\Promet;
use App\Dug;
use App\Potrazivanja;
use App\NacinPlacanja;
use App\Sektor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Session;

class PrometController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promet=Promet::orderBy('id','desc')->take(3)->get();
        return view ('promet_index',compact('promet'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $nacinplacanja=NacinPlacanja::all();
        $sektor=Sektor::all();
        return view ('dodavanje.promet',compact('nacinplacanja','sektor','stanje', 'old_promet'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $promet = Promet::create($request->all());
       //session()->flash('success', 'Promet uspješno snimljen!');
       return redirect ('/promet/add');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Promet  $promet
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {   
        $prometfilter=Promet::where('id','>',0)->get();
        $filterOD=0;
        $filter_np=$request->get('filter_np');
        $filter_s=$request->get('filter_s');
        $filter_ui=$request->get('filter_ui');
        $datumOD=$request->get('datumOD');
        $datumDO=$request->get('datumDO');
        $prikaz=$request->get('prikaz');
        $filter=$request->get('filter');
        //dd($datumOD);
        $nacinplacanja=NacinPlacanja::all();
        $sektor=Sektor::all();
        $promet=Promet::where('id','>',0);
        //dd($promet);
        if($filter_np>0) $promet->where('nacinplacanja_id', $filter_np);
        if($filter_s>0) $promet->where('sektor_id', $filter_s);
        if($filter_ui<>0) $promet->where('ulaz_izlaz', $filter_ui);
        //Greška za unos datuma
            if($datumDO==0){
                session()->flash('error', 'Greška pri unosu datuma!');
                return redirect()->action('PrometController@show');
            }  

            if($datumDO>$datumOD){
                
                if($datumOD!=0) $promet->whereDate('created_at','>=',($datumOD));
                if($datumDO!=0) $promet->whereDate('created_at','<=',($datumDO));

            }
            else{
                if($datumOD==0 || $datumDO==0)  session()->flash('error', 'Greška pri unosu datuma!');
                if($datumOD!=0) $promet->whereDate('created_at','>=',($datumDO));
                if($datumDO!=0) $promet->whereDate('created_at','<=',($datumOD));
            }
       //   dd($promet);
        $dug=Dug::all();
        $promet=$promet->orderBy('id','desc')->take($prikaz)->get();
        $stanje=DB::table('promets')->select(DB::raw('sum(ulaz_izlaz*iznos) as stanje'))->first();
        return view ('pregled_prometa',compact('promet','nacinplacanja','sektor','filter_np','filter_s','filter_ui','datumOD','datumDO','prikaz','stanje','filter','prometfilter','dug'));
    }

    public function show()
    {   
        $prometfilter=Promet::where('id','>',0)->get();
        $datumOD=0;
        $datumDO=date('Y-m-d');
        $filter=0;
        $nacinplacanja=NacinPlacanja::all();
        $sektor=Sektor::all();
        $promet=Promet::orderBy('id','desc')->take(10)->get();
        $dug=Dug::all();
        $stanje=DB::table('promets')->select(DB::raw('sum(ulaz_izlaz*iznos) as stanje'))->first();
        return view ('pregled_prometa',compact('pregled_prometa','promet','nacinplacanja','sektor','filter','stanje','datumOD','datumDO','prometfilter','dug'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Promet  $promet
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promet=Promet::find($id);
        $nacinplacanja=NacinPlacanja::all();
        $sektor=Sektor::all();
        return view ('edit',compact('promet','nacinplacanja','sektor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Promet  $promet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $promet=Promet::find($id);
        $promet->update($request->all());
        return redirect ('/stanje');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Promet  $promet
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Promet::destroy($id);
        return redirect ('/stanje');
    }

    public function changelog(){

        return view ('changelog');
    }
}
