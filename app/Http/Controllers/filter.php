<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Promet;
class filter extends Controller
{
    public function show(Request $request){
    	$filter_np=$request->get('filter_np');
    	$filter_s=$request->get('filter_s');
    	$filter_ui=$request->get('filter_ui');
    	$promet=Promet::where('nacinplacanja_id',$filter_np)->get();

    	return view ('filter',compact('filter_s','filter_ui','filter_np','promet'));

    }
}
