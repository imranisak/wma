<?php

namespace App\Http\Controllers;

use App\paket;
use App\hosting;
use Illuminate\Http\Request;

class PaketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paket=paket::where('id','>',0)->get();
        return view ('hosting.dodavanje.paket',compact('paket'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new=Paket::create($request->all());
        return redirect('/hosting/paket/add');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function show(paket $paket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paket=paket::find($id);
        return view ('hosting.edit.paket',compact('paket'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Paket=Paket::find($id);
        $Paket->update($request->all());
        session()->flash('success', 'Paket uspješno uređen');
        return redirect ('/hosting/paket/add');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $hosting=hosting::where('paket_id',$id)->get();
        if (count($hosting)>0){
            session()->flash('error','Ovaj paket je vec pridužen nekom hosting, tako da ne može biti obrisan');
        }
        else{
        paket::destroy($id);
        session()->flash('success', 'Paket obrisan');
        }
  
        return redirect ('/hosting/paket/add');
    }
}
