<?php

namespace App\Http\Controllers;

use App\domena;
use App\hosting;
use Illuminate\Http\Request;

class DomenaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $domena=domena::where('id','>',0)->get();
        return view ('hosting.dodavanje.domena',compact('domena'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new=Domena::create($request->all());
        return redirect()->route('dodajDomenu');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\domena  $domena
     * @return \Illuminate\Http\Response
     */
    public function show(domena $domena)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\domena  $domena
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $domena=domena::find($id);
        return view ('hosting.edit.domena',compact('domena'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\domena  $domena
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $domena=domena::find($id);
        $domena->update($request->all());
        session()->flash('success', 'Domena uspješno uređena');
        return redirect ('/hosting/domena/add');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\domena  $domena
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hosting=hosting::where('domena_id',$id)->get();
        if(count($hosting)>0){
             session()->flash('error','Ova domena je vec pridružena nekom hostingu, tako da ne može biti obrisana');
        }
        else{    
            domena::destroy($id);
            session()->flash('success', 'Domena obrisana');
        }
        return redirect ('/hosting/domena/add');
    }
}
