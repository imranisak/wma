<?php

namespace App\Http\Controllers;

use App\hosting;
use App\domena;
use App\paket;
use App\server;
use Illuminate\Http\Request;
use Session;
use DB;

class HostingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('hosting.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $server=Server::all();
        $domena=domena::all();
        $paket=paket::all();
        return view ('hosting.dodavanje.hosting',compact('server','domena','paket'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $hosting = hosting::create($request->all());
       session()->flash('success', 'Hosting uspješno snimljen');
       return redirect ('/hosting/add');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\hosting  $hosting
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   $filter=0;
        $domena=domena::all();
        $server=server::all();
        $paket=paket::all();
        $hosting=hosting::where('id','>',0)->orderBy('datum_obnove','asc')->get();
        //dd($hosting);
        return view ('hosting.pregled',compact('hosting','domena','server','paket','filter'));
    }

    public function filter(Request $request){
        $filter=$request->get('filter');
        $hosting=hosting::where('id','>',0);
        $server=server::all();
        $paket=paket::all();    
        $domena=domena::all();
        
        $serverfilter=$request->get('serverfilter');
        $paketfilter=$request->get('paketfilter');
        $domenafilter=$request->get('domenafilter');
        $prikaz=$request->get('prikaz');
        $datumOD=$request->get('datumOD');
        $datumDO=$request->get('datumDO');
        //dd($datumOD);

        if($serverfilter>0) $hosting->where('server_id',$serverfilter);
        if($paketfilter>0) $hosting->where('paket_id',$paketfilter);
        if($domenafilter>0) $hosting->where('domena_id',$domenafilter);

        //Filter DATUM
        if($datumDO!=0){
            if($datumDO==0){
                session()->flash('error', 'Greška pri unosu datuma');
                return redirect()->action('HostingController@show');
            }  
                if($datumDO>$datumOD){
                    
                    if($datumOD!=0) $hosting->whereDate('datum_obnove','>=',($datumOD));
                    if($datumDO!=0) $hosting->whereDate('datum_obnove','<=',($datumDO));

                }
                else{
                    if($datumOD==0 || $datumDO==0)  session()->flash('error', 'Greška pri unosu datuma!');
                    if($datumOD!=0) $hosting->whereDate('datum_obnove','>=',($datumDO));
                    if($datumDO!=0) $hosting->whereDate('datum_obnove','<=',($datumOD));
                }
        }
        if($prikaz==0) $hosting=$hosting->orderBy('id','desc')->get(); 
        else $hosting=$hosting->orderBy('id','desc')->take($prikaz)->get();
        return view ('hosting.pregled',compact('server','paket','domena','hosting','filter','serverfilter','paketfilter','domenafilter','prikaz','datumOD','datumDO'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\hosting  $hosting
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $hosting=hosting::find($id);
       // dd($hosting);
        $server=server::all();
        $domena=domena::all();
        $paket=paket::all();
        return view ('hosting.edit.hosting',compact('hosting','server','domena','paket'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\hosting  $hosting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hosting=hosting::find($id);
        $hosting->update($request->all());

        if($request->input('suspend')==1)
        {
            $hosting->suspend=1;
            $hosting->save();
        }
        else
        {
            $hosting->suspend=0;
            $hosting->save();          
        }  

        if($request->input('popust')==1)
        {
            $hosting->popust=1;
            $hosting->save();
        }
        else
        {
            $hosting->popust=0;
            $hosting->save();          
        }  

        if($request->input('statusPlacanja')==1)
        {
            $hosting->statusPlacanja=1;
            $hosting->save();
        }
        else
        {
            $hosting->statusPlacanja=0;
            $hosting->save();          
        }

        session()->flash('success', 'Hosting uspješno uređen');
        return redirect ('/hosting/show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\hosting  $hosting
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        hosting::destroy($id);
        session()->flash('success', 'Hosting obrisan');
        return redirect ('/hosting/show');
    }

    public function changelog(){
        return view ('hosting.changelog');

    }

    public function details($id){
        $hosting=hosting::find($id);
        return view('hosting.details',compact('hosting'));
    }

    public function ucitaj()
    {
        $data=hosting::all();
        //dd($data);
        return $data;
    }
}
