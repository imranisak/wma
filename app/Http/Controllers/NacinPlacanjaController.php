<?php

namespace App\Http\Controllers;

use App\NacinPlacanja;
use Illuminate\Http\Request;

class NacinPlacanjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view ('dodavanje.nacinplacanja'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new=NacinPlacanja::create($request->all());
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NacinPlacanja  $nacinPlacanja
     * @return \Illuminate\Http\Response
     */
    public function show(NacinPlacanja $nacinPlacanja)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NacinPlacanja  $nacinPlacanja
     * @return \Illuminate\Http\Response
     */
    public function edit(NacinPlacanja $nacinPlacanja)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NacinPlacanja  $nacinPlacanja
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NacinPlacanja $nacinPlacanja)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NacinPlacanja  $nacinPlacanja
     * @return \Illuminate\Http\Response
     */
    public function destroy(NacinPlacanja $nacinPlacanja)
    {
        //
    }
}
