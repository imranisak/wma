<?php

namespace App\Http\Controllers;

use App\server;
use App\hosting;
use Illuminate\Http\Request;

class ServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $server=server::where('id','>',0)->get();
        return view ('hosting.dodavanje.server',compact('server'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new=Server::create($request->all());
        return redirect()->route('dodajServer');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\server  $server
     * @return \Illuminate\Http\Response
     */
    public function show(server $server)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\server  $server
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $server=server::find($id);
        return view ('hosting.edit.server',compact('server'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\server  $server
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $server=server::find($id);
        $server->update($request->all());
        session()->flash('success','Server uspješno uređen');
        return redirect ('/hosting/server/add');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\server  $server
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $hosting=hosting::where('server_id',$id)->get();
        if (count($hosting)>0){
            session()->flash('error','Ovaj server je vec pridužen nekom hosting, tako da ne može biti obrisan');
        }
        else{
        server::destroy($id);
        session()->flash('success', 'Server obrisan');
        }
  
        return redirect ('/hosting/server/add');
    }
}
