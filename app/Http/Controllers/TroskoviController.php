<?php

namespace App\Http\Controllers;

use App\Troskovi;
use Illuminate\Http\Request;

class TroskoviController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fiksni.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Troskovi::create($request->all());
        session()->flash('success','Trošak dodan');
        return redirect ('/fiksni');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Troskovi  $troskovi
     * @return \Illuminate\Http\Response
     */
    public function show(Troskovi $troskovi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Troskovi  $troskovi
     * @return \Illuminate\Http\Response
     */
    public function edit(Troskovi $troskovi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Troskovi  $troskovi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Troskovi $troskovi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Troskovi  $troskovi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Troskovi $troskovi)
    {
        //
    }
}
