<?php

namespace App\Http\Controllers;

use App\Poruke;
use Illuminate\Http\Request;

class PorukeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $poruke=Poruke::where('id','>',0)
                        ->take(3)
                        ->orderBy('id','desc')
                        ->get();
        return response()->json($poruke);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $poruka=Poruke::create($request->all());
        return redirect('/hosting/show');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Poruke  $poruke
     * @return \Illuminate\Http\Response
     */
    public function show(Poruke $poruke)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Poruke  $poruke
     * @return \Illuminate\Http\Response
     */
    public function edit(Poruke $poruke)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Poruke  $poruke
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Poruke $poruke)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Poruke  $poruke
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Poruke::destroy($id);
        return redirect('/hosting/show');
    }
}
