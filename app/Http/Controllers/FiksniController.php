<?php

namespace App\Http\Controllers;

use App\Fiksni;
use Illuminate\Http\Request;

class FiksniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('fiksni.pregled');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fiksni  $fiksni
     * @return \Illuminate\Http\Response
     */
    public function show(Fiksni $fiksni)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fiksni  $fiksni
     * @return \Illuminate\Http\Response
     */
    public function edit(Fiksni $fiksni)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fiksni  $fiksni
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fiksni $fiksni)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fiksni  $fiksni
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fiksni $fiksni)
    {
        //
    }
}
