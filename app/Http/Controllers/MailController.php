<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\hosting;
use App\Mail\HostingMail;
use Session;
use Mail;

class MailController extends Controller
{
    public function sendMail()
    {
        $hosting=hosting::where('id','>',0);
        $danDanas=date('Y-m-d');
        $zaPetnaestDana=date('Y-m-d', strtotime($danDanas. ' + 15 days'));//Doda 15 dana na danasni dan
        $hosting=$hosting->whereDate('datum_obnove','<=',($zaPetnaestDana));
        $hosting=$hosting->whereDate('datum_obnove','>=',($danDanas))->get();
        foreach ($hosting as $hosting) 
        {
            $podaciZaSlanje=array();
            $podaciZaSlanje = array(
                'klijent' =>$hosting['kontakt'] , 
                'domena' =>$hosting['naziv_domene'],
                'mail' =>$hosting['mail']  
            );
        Mail::to('web.webmedia@gmail.com')->queue(new HostingMail($hosting));
            

        }

        /*$podaciZaSlanje=array(
            'klijent'=>$hosting['kontakt'],
            'domena'=>$hosting['naziv_domene'],
            'mail'=>$hosting['mail']
        );*/
        //dd($hosting);
        /*Mail::send('emails.mail',$podaciZaSlanje, function($message) use ($podaciZaSlanje)
            {
                $message->from('web.webmediamail@gmail.com');
                $message->to($podaciZaSlanje['mail']);
            });*/
        Session::flash('success',"Mail sent!");
    }
}
