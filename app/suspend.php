<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class suspend extends Model
{
    protected $fillable=['suspend'];
}
