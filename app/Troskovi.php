<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Troskovi extends Model
{
    protected $fillable = ['trosak'];
}
