<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poruke extends Model
{
    protected $fillable = ['poruka','od','za','datumObjave'];
}
