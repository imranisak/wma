<?php

namespace App\Mail;

use App\hosting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class HostingMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $hosting;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(hosting $hosting)
    {
        $this->hosting=$hosting;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.mail')
                    ->with(['klijent'=>$this->hosting->kontakt,
                            'domena'=>$this->hosting->naziv_domene,
                            ]);
    }
}
