<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hosting extends Model
{
    protected $fillable = ['naziv_domene','domena_id','server_id','kontakt','mail','telefon','datum_obnove','paket_id','cijena','napomena','statusPlacanja', 'suspend', 'popust','usluga'];

    public function server(){
    	return $this->belongsTo(server::class);
    }
    public function domena(){
    	return $this->belongsTo(domena::class);
    }  
    public function paket(){
    	return $this->belongsTo(paket::class);
    } 
}