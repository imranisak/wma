<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sektor extends Model
{
    protected $fillable = ['sektor'];
}
