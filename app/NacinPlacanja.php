<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NacinPlacanja extends Model
{
    protected $fillable = ['nacinplacanja'];
}
