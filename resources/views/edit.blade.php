@extends('master')
@section ('head')
    <title>Uredi promet</title>

   @include('partials.head-asset')
@endsection

@section ('content')
    <h1>Uredi promet</h1>
    <form method="post" action="/promet/edit/{{$promet->id}}">   
    {{csrf_field()}}  

    Način plaćanja: 
    <br>
      <div class="form-group">
            <div class="col-lg-2">
                <select name="nacinplacanja_id" id="basic" class="selectpicker show-tick form-control">
                 @foreach($nacinplacanja as $np)
                <option value="{{$np->id}}"  @if($np->id==$promet->nacinplacanja_id) selected @endif>{{$np->nacinplacanja}}</option>
                @endforeach
                </select>
            </div>
        </div>
        <br>
        <br>

    Sektor:
    <br>
    <div class="form-group">
        <div class="col-lg-2">
             <select name="sektor_id" id="basic" class="selectpicker show-tick form-control">
              @foreach($sektor as $s)
             <option value="{{$s->id}}" @if($s->id==$promet->sektor_id) selected @endif> {{$s->sektor}} </option>
             @endforeach
            </select>
        </div>
    </div>
    <br>

    Ulaz / izlaz:
    <div class="form-group">
        <div class="col-lg-2">
            <select name="ulaz_izlaz" id="basic" class="selectpicker show-tick form-control">
                <option value="1" @if($promet->ulaz_izlaz==1) selected @endif>Ulaz</option>
                <option value="-1" @if($promet->ulaz_izlaz==-1) selected @endif>Izlaz</option>
            </select>
        </div>
    </div>
    <br>

    Iznos:
    Iznos:
    <div class="form-group">
        <div class="col-lg-2">
          <input type="number" name="iznos" class="form-control" value="{{$promet->iznos}}">
        </div>
    </div><br>

    Napomena:
    <div class="form-group">
        <div class="col-lg-4">
          <input type="text" name="napomena" class="form-control" value="{{$promet->napomena}}">
        </div>
    </div><br>
    <br>

    <input type="submit" value="Uredi promet" type="button" class="btn btn-success">
    </form>
@endsection