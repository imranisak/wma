<span> Dugovanja: </span> <br>
@php
$suma=0;
$potrazivanja=0;
$dugg=0;
@endphp

@foreach ($dug as $data)
	@php
		if($data->tip==1){
		$potrazivanja+=$data->iznos;
		$suma+=$data->iznos;
		}

		if($data->tip==-1){
		$dugg+=$data->iznos;
		$suma-=$data->iznos;
		}
	@endphp
@endforeach	

	Dug: <span style="color: red">  @php echo $dugg; @endphp</span> Potraznja: <span style="color: blue" >@php echo $potrazivanja; @endphp</span> Ukupno: <span style="color: green"> @php echo $suma; @endphp</span>

	<form method="post" action="/dug/add">
		{{csrf_field()}}
			<label>Iznos</label>
			<input type="number" name="iznos">

			<label>Vrsta</label>	
			<select name="tip">
				<option value="1">Potražnja</option>
				<option value="-1">Dug</option>
			</select>
			<input type="submit" value="Snimi" class="btn btn-primary">
	</form>	