<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div id="poruke">
  <div id="porukeHeader">Notes</div>
	<div id="porukaSadrzaj">
	</div>
	<div id="unosPoruke">
		<form method="POST" action="/poruke/dodaj">
			{{csrf_field()}}
			<input type="text" name="poruka" placeholder="Unesi poruku" required><br>
			<input type="text" name="od" placeholder="Objavio:" required><br>
			<input type="text" name="za" placeholder="Za:" required><br>
			<input type="date" name="datumObjave" required><br>
			<input type="submit">
		</form>
	</div>
</div>

<script type="text/javascript">
	
$(document).ready(function(){
      $.ajax({
        type: 'GET',
        url: '/poruke/ucitaj',
        success: function(data)
        {	
        	var duzinaNiza=data.length;
        	for (var i=0; i<duzinaNiza;i++) 
        	{
        		var porukaDiv=$("<div id='poruka'></div>"),
        			poruka=data[i].poruka,
        			od=data[i].od,
        			za=data[i].za,
        			datumObjave=data[i].datumObjave;

        		var porukaSadrzaj="Poruka:"+poruka+"<br>";
        		$(porukaDiv).append(porukaSadrzaj);

        		var objavioSadrzaj="Objavio:"+od+"<br>";
        		$(porukaDiv).append(objavioSadrzaj);

        		var zaSadrzaj="Za:"+za+"<br>";
        		$(porukaDiv).append(zaSadrzaj);

        		var datumObjaveSadrzaj="Objavljeno:"+datumObjave+"<br><br>";
        		$(porukaDiv).append(datumObjaveSadrzaj);

        		var linkZaBrisanjePoruke="<a href='/poruke/obrisi/"+data[i].id+"'>Obrisi poruku</a>";
        		console.log("<a href='/poruke/obrisi/'"+data[i].id+">Obrisi poruku</a>");
        		$(porukaDiv).append(linkZaBrisanjePoruke);

        		var linija="<hr>";
        		$(porukaDiv).append(linija);

        		$("#porukaSadrzaj").append(porukaDiv);
        	}
        }
      })
});
</script>