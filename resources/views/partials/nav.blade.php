<nav class="nav nav-pills">
  <div class="container-fluid" >
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
      </button>
      <a class="navbar-brand" href="/finansije/index">Web Media Finansije <sub>v1.1</sub></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
    	
        <li>
          <div class="btn-group">
            <a href="/promet/add">
          <button type="button" class="btn btn-success" style="margin-top: 6px">
          Dodaj promet 
          </button>
          </a>   
      </div> &nbsp         
    </li>        

    <li>
          <div class="btn-group">
            <a href="/stanje">
            <button type="button" class="btn btn-success" style="margin-top: 6px">
            Prikaži stanje
            </button>
            </a>
          </div>  &nbsp
    </li> 
    <li>
          <div class="btn-group">
            <a href="/nacinplacanja/add">
          <button type="button" class="btn btn-primary" style="margin-top: 6px">
          Dodaj način plaćanja
          </button>
          </a>
      </div>  &nbsp
    </li>

    <li>
	        <div class="btn-group">
	        	<a href="/sektor/add">
			  	<button type="button" class="btn btn-primary" style="margin-top: 6px">
			    Dodaj sektor
			    </button>
			    </a>
			</div>	&nbsp
		</li>

    <li >&nbsp
          <div class="btn-group pull-right">
            <a href="/changelog">
          <button type="button" class="btn btn-primary" style="margin-top: 6px;right:110px;">
          Changelog
          </button>
          </a>
      </div>  
    </li>    

    <li >&nbsp
          <div class="btn-group pull-right">
            <a href="/hosting">
          <button type="button" class="btn btn-primary" style="margin-top: 6px;right:110px;">
          Hosting
          </button>
          </a>
      </div>  
    </li>
    <li >&nbsp
          <div class="btn-group">
            <a href="/logout">
          <button type="button" class="btn btn-danger" style="margin-top: 6px">
          Logout
          </button>
          </a>
        </div>  
    </li>    
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>