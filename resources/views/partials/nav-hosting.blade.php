<nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm">
    <div class="container">
    <a class="navbar-brand" href="/"><img src="https://demo4.webmedia.ba/wp-content/uploads/2018/09/logotip-webmedia-1.png"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <li class="{{ Request::is('hosting') ? 'active' : '' }} nav-item">
              <a class="nav-link" href="/hosting">Pregled po mjesecu</a>
        </li>
        <li class="{{ Request::is('hosting/show') ? 'active' : '' }} nav-item">
          <a class="nav-link" href="/hosting/show">Pregled hostinga</a>
        </li>
        <li class="{{ Request::is('hosting/add') ? 'active' : '' }} nav-item">
            <a class="nav-link" href="/hosting/add">Dodaj Hosting</a>
        </li>
        <li class="{{ Request::is('hosting/server/add') ? 'active' : '' }} nav-item">
            <a class="nav-link" href="/hosting/server/add">Dodaj server</a>
        </li>
        <li class="{{ Request::is('hosting/paket/add') ? 'active' : '' }} nav-item">
          <a class="nav-link" href="/hosting/paket/add">Dodaj paket</a>
        </li>
        <li class="{{ Request::is('hosting/domena/add') ? 'active' : '' }} nav-item">
          <a class="nav-link" href="/hosting/domena/add">Dodaj domenu</a>
        </li>
        <li class="{{ Request::is('logout') ? 'active' : '' }} nav-item">
          <a class="nav-link btn btn-danger text-white" href="/logout">Odjava <i class="fas fa-sign-out-alt"></i></a>
        </li>
      </ul>
    </div>
  </div>
  </nav>