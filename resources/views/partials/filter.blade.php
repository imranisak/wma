<form method="post"	action="/stanje">
		{{csrf_field()}}
		<div class="form-group">
			<div class="col-lg-2">
				<label>Nacin plaćanja</label>
				<select name="filter_np" id="basic" class="selectpicker show-tick form-control">
					<option value="0" @if($filter==1) @if($filter_np==0) selected @endif @endif >Sve</option>
					@foreach($nacinplacanja as $np)
					<option value="{{$np->id}}" @if($filter==1) @if($np->id==$filter_np) selected @endif @endif >{{$np->nacinplacanja}}</option>
					@endforeach
				</select>	
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-2">	
				<label>Sektor</label>
				<select name="filter_s" id="basic" class="selectpicker show-tick form-control">
					<option value="0" @if($filter==1) @if($filter_s==0) selected @endif @endif>Sve</option>
					@foreach($sektor as $s)
					<option value="{{$s->id}}" @if($filter==1) @if($s->id==$filter_s) selected @endif @endif>{{$s->sektor}}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-1">
				<label>Ulaz/Izlaz</label>
				<select name="filter_ui" select class="selectpicker show-thin form-control">
					<option value="0" @if($filter==1) @if($filter_ui==0) selected @endif @endif>Sve</option>
					<option value="1" @if($filter==1) @if($filter_ui==1) selected @endif @endif>Ulaz</option>
					<option value="-1" @if($filter==1) @if($filter_ui==-1) selected @endif @endif>Izlaz</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-2">
				<label>OD</label>
				<input type="date" name="datumOD" class="moje-input" @if($filter==1) value="{{$datumOD}}" @endif>	
			</div>
		</div>

		<div class="form-group">
			<div class="col-lg-2">
				<label>DO</label>
				<input type="date" name="datumDO" class="moje-input" @if($filter==1) value="{{$datumDO}}" @else value="@php echo date('Y-m-d') @endphp" @endif>
			</div>
		</div>

	    <div class="form-group">
	        <div class="col-lg-1">
	          <label>Prikaži:</label>
	          <input type="number" name="prikaz" class="form-control" @if($filter==1) value="{{$prikaz}}" @else value="10" @endif>
	        </div>
	    </div>

	    <input type="number" name="filter" value="1" hidden>
			<input type="submit" type="button" value="Filtriraj" class="btn btn-primary">
</form>
			@if($filter==1)
			<a href="/stanje"><button class="btn btn-danger">Resetuj filter</button></a>
			@endif