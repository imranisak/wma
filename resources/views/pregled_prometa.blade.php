@extends('master')
@section ('head')
	<title>Prikaz prometa</title>
@include('partials.head-asset')
<script language="JavaScript" type="text/javascript">
	$(document).ready(function(){
	    $("a.delete").click(function(e){
	        if(!confirm('Obrisati transakciju!?')){
	            e.preventDefault();
	            return false;
	        }
	        return true;
	    });
	});
</script>

@endsection

@section ('content')
	<h1>Prikaz prometa</h1>
	@if($stanje->stanje>0)
	<h3>Trenutno stanje računa je: <span style="color: green">{{$stanje->stanje}}</span></h3>
	@else
	<span><h3>Trenutno stanje računa je: <span style="color: red">{{$stanje->stanje}}</span></h3></span>	
	@endif

	@include('partials.za_mjesec')
	@include('partials.dug')
	@include('partials.filter')
<!--TABELA-->
<div class="col-md-12">	
		<table class="table-hover table-responsive col-md-12" border="2">
			<tr>
				<th>Vrsta transakcije</th>
				<th>Sektor</th>
				<th>Način plaćanja</th>
				<th>Iznos</th>
				<th>Napomena</th>
				<th>Uneseno na</th>
				<th>Opcije</th>
			</tr>
			@php $total=0; @endphp
			@foreach($promet as $pr)
					<tr>
						<td >@if($pr->ulaz_izlaz==1) Ulaz @else Izlaz @endif</td>
						<td>{{$pr->sektor->sektor}}</td>
						<td >{{$pr->nacinplacanja->nacinplacanja}}</td>
						<td @if($pr->ulaz_izlaz==1) style="background-color:rgba(0,255,0,0.5);" @else style="background-color:rgba(255,0,0,0.5);" @endif >{{$pr->iznos}} @php if($pr->ulaz_izlaz==1) $total+=$pr->iznos; else $total-=$pr->iznos; @endphp</td>
						<td >{{$pr->napomena}}</td>
						<td>@php echo date('d-m-Y', strtotime($pr->created_at));   @endphp</td>
						<td >

							<a href="/promet/edit/{{$pr->id}}"> <span class="fa fa-edit"></span></span></a> |
							<a href="/promet/delete/{{$pr->id}}" class="delete"><span class="fa fa-remove text-danger"></span></a>
						</td>
					</tr>
			@endforeach
			<tr>
				<td  colspan="8" align="right"><b>Total:{{$total}}</b></td>

			</tr>
		</table>	
	<button onclick="printaj('zaprintati')" class="btn btn-default">PRINT</button>
</div>

<!--TABELA ZA PRINTANJE-->
<div class="col-md-12" id="zaprintati" hidden>	
		<table class="table-hover table-responsive col-md-12" border="2">
			<tr>
				<th>Vrsta transakcije</th>
				<th>Sektor</th>
				<th>Način plaćanja</th>
				<th>Iznos</th>
				<th>Napomena</th>
				<th>Uneseno na</th>
			</tr>
			@php $total=0; @endphp
			@foreach($promet as $pr)
					<tr>
						<td >@if($pr->ulaz_izlaz==1) Ulaz @else Izlaz @endif</td>
						<td>{{$pr->sektor->sektor}}</td>
						<td >{{$pr->nacinplacanja->nacinplacanja}}</td>
						<td @if($pr->ulaz_izlaz==1) style="background-color:rgba(0,255,0,0.5);" @else style="background-color:rgba(255,0,0,0.5);" @endif >{{$pr->iznos}} @php if($pr->ulaz_izlaz==1) $total+=$pr->iznos; else $total-=$pr->iznos; @endphp</td>
						<td >{{$pr->napomena}}</td>
						<td>{{$pr->created_at}}</td>
					</tr>
			@endforeach
			<tr>
				<td  colspan="8" align="right"><b>Total:{{$total}}</b></td>

			</tr>
		</table>	

		Izvještaj za period od @if($datumOD!=0){{$datumOD}} @else - @endif do @if($datumDO!=0){{$datumDO}} @else -  @endif
</div>
@endsection


	
<!-- SKRIPTA ZA PRINTANJE-->
<script>
	function printaj(zaprintati) {
	     var printContents = document.getElementById(zaprintati).innerHTML;
	     var originalContents = document.body.innerHTML;

	     document.body.innerHTML = printContents;

	     window.print();

	     document.body.innerHTML = originalContents;
	}
</script>
