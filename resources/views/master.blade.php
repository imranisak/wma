<!DOCTYPE html>
<html>
<head>
	@yield('head')
</head>
<body>
<div class="container">
	<div class="col-md-13">
				@include('partials.nav')
				@yield('nav')
				@yield('content')
	</div>
</div>

</body>
</html>