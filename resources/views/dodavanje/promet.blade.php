@extends('master')
@section ('head')
    <title>Dodaj promet</title>
      <script src="dist/js/bootstrap-select.js"></script>

@include('partials.head-asset')
@endsection

@section ('content')
    <h1>Dodaj promet</h1>
    <form method="post" action="/promet/add"  class="form-horizontal" role="form">   
    {{csrf_field()}}  
    <br>

    Način plaćanja: 
      
        <div class="form-group">
            <div class="col-lg-2">
                <select name="nacinplacanja_id" id="basic" class="selectpicker show-tick form-control">
                 @foreach($nacinplacanja as $np)
                <option value="{{$np->id}}">{{$np->nacinplacanja}}</option>
                @endforeach
                </select>
            </div>
        </div>

    Sektor:
    <div class="form-group">
         <div class="col-lg-2">
            <select name="sektor_id" id="basic" class="selectpicker show-tick form-control">
                @foreach($sektor as $s)
                <option value="{{$s->id}}">{{$s->sektor}}</option>
                @endforeach
            </select>
        </div>
    </div>

    Ulaz / izlaz:
    <div class="form-group">
        <div class="col-lg-2">
            <select name="ulaz_izlaz" id="basic" class="selectpicker show-tick form-control">
                <option value="1">Ulaz</option>
                <option value="-1">Izlaz</option>
            </select>
        </div>
    </div>

    Iznos:
    <div class="form-group">
        <div class="col-lg-2">
          <input type="number" name="iznos" class="form-control" step="0.01">
        </div>
    </div>

    Napomena:
    <div class="form-group">
        <div class="col-lg-4">
          <input type="text" name="napomena" class="form-control">
        </div>
    </div>

    <input type="submit" value="Dodaj promet" type="button" class="btn btn-success">
    </form>

@endsection