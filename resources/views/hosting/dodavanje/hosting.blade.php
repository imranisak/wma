@extends('master-hosting')
@section ('head')
    <title>Dodaj hosting</title>
          <script src="dist/js/bootstrap-select.js"></script>
@include('partials.head-asset')
@endsection

@section ('content')
<h1>Dodaj hosting</h1>
<form method="post" action="/hosting/add" class="form-horizontal" role="form">
	{{csrf_field()}}
	Naziv domene:*
	<div class="form-group">
		<div class="col-lg-3">
			<input type="text" name="naziv_domene" class="form-control" required>
		</div>
	</div>

	Domena:*
	<div class="form-group">
		<div class="col-lg-3">
			<select name="domena_id"  class="selectpicker show-tick form-control" required>
				<option disabled selected>Izaberi domenu</option>
				@foreach($domena as $data)
				<option value="{{$data->id}}">{{$data->domena}}</option>
				@endforeach
			</select>
		</div>
	</div>	

	Server:*
	<div class="form-group">
		<div class="col-lg-3">
			<select name="server_id" class="selectpicker show-tick form-control" required>
				<option disabled selected>Izaberi server</option>
				@foreach($server as $data)
				<option  value="{{$data->id}}">{{$data->server}}</option>
				@endforeach
			</select> 
		</div>
	</div>

	Paket:*
	<div class="form-group">
		<div class="col-lg-3">
			<select name="paket_id" class="selectpicker show-tick form-control" required>
				<option disabled selected>Izaberi paket</option>
				@foreach($paket as $data)
				<option  value="{{$data->id}}">{{$data->paket}}</option>
				@endforeach
			</select>
		</div>
	</div>

	Ime i prezime: 
	<div class="form-group">
		<div class="col-lg-3">
			<input type="text" name="kontakt" class="form-control">
		</div>
	</div>


	E-mail: 
	<div class="form-group">
		<div class="col-lg-3">
			<input type="email" name="mail" class="form-control">
		</div>
	</div>


	Broj telefona:
	<div class="form-group">
		<div class="col-lg-3">
			<input class="form-control" type="text" name="telefon">
		</div>
	</div>

	Datum obnove:
	<div class="form-group">
		<div class="col-lg-3">
			<input class="form-control" type="date" name="datum_obnove" value="@php echo date('Y-m-d'); @endphp" required>
		</div>
	</div>

	Cijena:
	<div class="form-group">
		<div class="col-lg-3">
			<input type="number" name="cijena" step="0.01" class="form-control">
		</div>
	</div>

	Napomena:
    <div class="form-group">
        <div class="col-lg-4">
          <input type="text" name="napomena" class="form-control">
        </div>
    </div>	

	Usluga:
    <div class="form-group">
        <div class="col-lg-4">
          <input type="text" name="usluga" class="form-control">
        </div>
    </div>	 

    <div class="form-check">
	  <input class="form-check-input" type="checkbox" value="1" id="statusPlacanjastatusPlacanja" name="statusPlacanja">
	  	<label class="form-check-label" for="statusPlacanja">
			Plaćeno
			</label>
	</div>    

	<div class="form-check">
	  <input class="form-check-input" type="checkbox" value="1" id="suspendovan" name="suspend">
	  	<label class="form-check-label" for="suspendovan">
			Suspendovan
			</label>
	</div>

	<input type="submit" class="btn btn-primary">
</form>

@endsection