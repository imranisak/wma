@extends('master-hosting')
@section ('head')
    <title>Dodaj server</title>
@include('partials.head-asset')
@endsection

@section ('content')
<h1>Dodaj server</h1>
<form method="post" action="/hosting/server/add" class="form-horizontal" role="form">
	{{csrf_field()}}
	<div class="form-group">
		<div class="col-lg-2">
			<input type="text" name="server" class="form-control" required><br>
			<input type="submit" class="btn btn-primary" value="Dodaj">
		</div>
	</div>
</form>
@foreach($server as $data)
{{$data->server}} <a href="/hosting/server/edit/{{$data->id}}">Uredi</a> <a href="/hosting/server/delete/{{$data->id}}">Obriši</a> <br>
@endforeach
@endsection