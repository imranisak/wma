@extends('master-hosting')
@section ('head')
    <title>Dodaj paket</title>
@include('partials.head-asset')
@endsection

@section ('content')
<h1>Dodaj paket</h1>
<form method="post" action="/hosting/paket/add" class="form-horizontal" role="form">
	{{csrf_field()}}
	<div class="form-group">
		<div class="col-lg-2">
			<input type="text" name="paket" class="form-control" required><br>
			<input type="submit" class="btn btn-primary" value="Dodaj">
		</div>
	</div>
</form>
<hr>
@foreach($paket as $data)
{{$data->paket}} <a href="/hosting/paket/edit/{{$data->id}}">Uredi </a> <a href="/hosting/paket/delete/{{$data->id}}">Obriši</a>      <br>
@endforeach

@endsection