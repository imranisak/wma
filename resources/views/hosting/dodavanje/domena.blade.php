@extends('master-hosting')
@section ('head')
    <title>Dodaj domenu</title>
@include('partials.head-asset')
@endsection

@section ('content')
<h1>Dodaj domenu</h1>
<form method="post" action="/hosting/domena/add" class="form-horizontal" role="form">
	{{csrf_field()}}
	<div class="form-group">
		<div class="col-lg-2">
			<input type="text" name="domena" class="form-control" required><br>
			<input type="submit" class="btn btn-primary" value="Dodaj">
		</div>
	</div>
</form>
<hr>
@foreach($domena as $data)
{{$data->domena}} <a href="/hosting/domena/edit/{{$data->id}}">Uredi </a> <a href="/hosting/domena/delete/{{$data->id}}">Obriši</a><br>
@endforeach

@endsection