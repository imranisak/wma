@extends('master-hosting')
@section ('head')
    <title>Pregled hostinga</title>
@include('partials.head-asset')
@endsection


@section ('content')
<a class="btn btn-danger d-inline text-white" href="/hosting">Nazad</a>
<h3 class="mt-2 mb-4 text-center">Pregled hostinga</h3>
				@php 
					$now=0;
					$now=strtotime(date('Y-m-d'));
				@endphp
	@include('hosting.filter')

	@php
	$br=1;
	$pr=1;
	@endphp
<br>

	<table id="main" class="table table-hover">
		<tr>
		<thead>
			<th></th>
			<th>Naziv domene</th>
			<th>Domena</th>
			<th>Server</th>
			<th>Paket</th>
			<th>Kontakt</th>
			<th>Dodatna usluga</th>
			<th>Cijena</th>
			<th>Status placanja</th>
			<th>Datum obnove</th>
			<th>Napomena</th>
			<th>Opcije</th>
		</tr>
		</thead>
		
		<tbody id="tabela">
			@foreach($hosting as $data)
			<tr <?php if($data->suspend==1) echo "style='background-color:rgba(250,0,0,0.3)'"; ?>  >
			@php 
			$note=$data->napomena;
			@endphp
				<td style="padding-left: 4px">@php echo $br++; @endphp</td>
				<td><a href="http://{{$data->naziv_domene}}{{$data->domena->domena}}" target="_blank">{{$data->naziv_domene}}</a></td>
				<td>{{$data->domena->domena}}</td>
				<td>{{$data->server->server}}</td>
				<td>{{$data->paket->paket}}</td>
				<td>
				{{$data->kontakt}}<br>
				{{$data->mail}}<br>
				{{$data->telefon}}
				</td>
				<td>{{$data->usluga}}</td>
				<td>{{$data->cijena}}</td>
				<div>
				<!--Prvi broj u span clasi oznacava status, dok drugi je ID -->
				<td class="statusPlacanja" >@if($data->statusPlacanja==1)<span class="fa fa-check text-success 1-<?php echo $data->id ?>"></span>@endif
					@if($data->statusPlacanja==0)<span class="fa fa-remove text-danger 0-<?php echo $data->id ?>"></span>@endif
				</td>
				</div>
				<td>@php echo date('m-y', strtotime($data->datum_obnove));@endphp</td>
				<td>@php if (strlen($note) > 30) echo"..."; else echo $data->napomena; @endphp</td>
				<td><a href="/hosting/edit/{{$data->id}}"> <span class="fa fa-edit"></span></a> |
							 <a href="/hosting/delete/{{$data->id}}" class="delete"><span class="fa fa-remove text-danger "></span></a></td>
			</tr>
			@endforeach
		
		</tbody>
	</table>
	<button onclick="printaj('zaprintanje')" class="btn btn-success">PRINT</button>
<!--ZA PRINTATI -->

<div id="zaprintanje" hidden>
	<table border="2">
		<tr>
			<th></th>
			<th>Naziv domene</th>
			<th>Domena</th>
			<th>Server</th>
			<th>Ime i prezime</th>
			<th>E-Mail</th>
			<th>Broj tel.</th>
			<th>Datum obnove</th>
			<th>Paket</th>
			<th>Cijena</th>
			<th>Napomena</th>
		</tr>
		@foreach($hosting as $data)
		<tr>
			<td>@php echo $pr++; @endphp</td>
			<td>{{$data->naziv_domene}}</td>
			<td>{{$data->domena->domena}}</td>
			<td>{{$data->server->server}}</td>
			<td>{{$data->kontakt}}</td>
			<td>{{$data->mail}}</td>
			<td>{{$data->telefon}}</td>
			<td @if((strtotime($data->datum_obnove) - $now)/86400 <=15) style="background-color:rgba(255,0,0,0.5);"@endif> {{$data->datum_obnove}} @if((strtotime($data->datum_obnove) - $now)/86400 <=0) <b>ISTEKLO</b>@endif</td>
			<td>{{$data->paket->paket}}</td>
			<td>{{$data->cijena}}</td>
			<td>{{$data->napomena}}</td>
		</tr>
		@endforeach
	</table>
</div>
	<!-- SKRIPTA ZA PRINTANJE-->
<script>
	function printaj(zaprintanje) {
	     var printContents = document.getElementById(zaprintanje).innerHTML;
	     var originalContents = document.body.innerHTML;

	     document.body.innerHTML = printContents;

	     window.print();

	     document.body.innerHTML = originalContents;
	}
</script>
</div>
@include('partials.footer')
<script language="JavaScript" type="text/javascript">
	$(document).ready(function(){
	    $("a.delete").click(function(e){
	        if(!confirm('Obrisati hosting!?')){
	            e.preventDefault();
	            return false;
	        }
	        return true;
	    });
	});
</script>
<script>
	$(document).ready(function (){
  $("#domenaTrazilica").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#tabela tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });  
});
</script>

<script type="text/javascript">
//Kod za izmjenu statusa placanja
$(".statusPlacanja").click(function()
{
	var klasa=($(this).children().attr("class")),
			statusPlacanja,
			statusPlacanjaiID,
			id,
			element=$(this);
	statusPlacanjaiID=klasa.split(" ");//Rastavi klasu u niz rijeci
	statusPlacanjaiID=statusPlacanjaiID[statusPlacanjaiID.length-1];//Zadnja rijec je u obliku statusPlacanja-ID
	statusPlacanjaiID=statusPlacanjaiID.split("-");
	id=statusPlacanjaiID[1];
	statusPlacanja=statusPlacanjaiID[0];
	$.ajax({
		type: "get",
		data: {sp:statusPlacanja,idHostinga:id},
		url: "/statusPlacanja",
		success: function(response)
		{
			if(response==1)
			{
				var placeno="<span class='fa fa-check text-success "+1+"-"+id+"'"+"></span>";
				element.html(placeno);
			}
			else
			{
				var nijePlaceno="<span class='fa fa-remove text-danger "+0+"-"+id+"'"+"></span>";
				element.html(nijePlaceno);
			}
		},
		error: function()
		{
			alert("Greska. Aj ponovo.");
		}
	})
});
</script>
@endsection
