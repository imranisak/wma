@extends('master-hosting')
@section ('head')
    <title>Web Media hosting</title>
    <style>
    	.dugme {
			width:100px;
		}
    </style>
@include('partials.head-asset')
@endsection

@section ('content')
    <h3 class="text-center mt-5 mb-5">Odaberi hosting po mjesecu</h3>

			<div class="d-flex flex-wrap">
			<div class="col-md-2 mt-2 col-6">
   			<form action="/hosting/show" method="post" >{{csrf_field()}}<input type="date" name="datumOD" value="@php echo date('Y-01-01') @endphp" hidden> <input type="date" name="datumDO" value="@php echo date('Y-01-31') @endphp" hidden><input type="submit" value="Januar" type="button" class="btn dugme btn-primary"><input type="number" name="filter" value="1" hidden></form>
			</div>

			<div class="col-md-2 mt-2 col-6">
   			<form action="/hosting/show" method="post" >{{csrf_field()}}<input type="date" name="datumOD" value="@php echo date('Y-02-01') @endphp" hidden> <input type="date" name="datumDO" value="@php echo date('Y-02-28') @endphp" hidden><input type="submit" value="Februar" type="button" class="btn dugme btn-primary"><input type="number" name="filter" value="1" hidden></form>
			</div>
			<div class="col-md-2 mt-2 col-6">
   			<form action="/hosting/show" method="post" >{{csrf_field()}}<input type="date" name="datumOD" value="@php echo date('Y-03-01') @endphp" hidden> <input type="date" name="datumDO" value="@php echo date('Y-03-31') @endphp" hidden><input type="submit" value="Mart" type="button" class="btn dugme btn-primary"><input type="number" name="filter" value="1" hidden></form>
			</div>
			<div class="col-md-2 mt-2 col-6">
   			<form action="/hosting/show" method="post" >{{csrf_field()}}<input type="date" name="datumOD" value="@php echo date('Y-04-01') @endphp" hidden> <input type="date" name="datumDO" value="@php echo date('Y-04-30') @endphp" hidden><input type="submit" value="April" type="button" class="btn dugme btn-primary"><input type="number" name="filter" value="1" hidden></form>
			</div>
			<div class="col-md-2 mt-2 col-6">
   			<form action="/hosting/show" method="post" >{{csrf_field()}}<input type="date" name="datumOD" value="@php echo date('Y-05-01') @endphp" hidden> <input type="date" name="datumDO" value="@php echo date('Y-05-31') @endphp" hidden><input type="submit" value="Maj" type="button" class="btn dugme btn-primary"><input type="number" name="filter" value="1" hidden></form>
			</div>   
			<div class="col-md-2 mt-2 col-6">
   			<form action="/hosting/show" method="post" >{{csrf_field()}}<input type="date" name="datumOD" value="@php echo date('Y-06-01') @endphp" hidden> <input type="date" name="datumDO" value="@php echo date('Y-06-30') @endphp" hidden><input type="submit" value="Juni" type="button" class="btn dugme btn-primary"><input type="number" name="filter" value="1" hidden></form>
			</div>
			<div class="col-md-2 mt-2 col-6">
   			<form action="/hosting/show" method="post" >{{csrf_field()}}<input type="date" name="datumOD" value="@php echo date('Y-07-01') @endphp" hidden> <input type="date" name="datumDO" value="@php echo date('Y-07-31') @endphp" hidden><input type="submit" value="Juli" type="button" class="btn dugme btn-primary"><input type="number" name="filter" value="1" hidden></form>
			</div>
			<div class="col-md-2 mt-2 col-6">
   			<form action="/hosting/show" method="post" >{{csrf_field()}}<input type="date" name="datumOD" value="@php echo date('Y-08-01') @endphp" hidden> <input type="date" name="datumDO" value="@php echo date('Y-08-31') @endphp" hidden><input type="submit" value="August" type="button" class="btn dugme btn-primary"><input type="number" name="filter" value="1" hidden></form>
			 </div>  
			<div class="col-md-2 mt-2 col-6">
   			<form action="/hosting/show" method="post" >{{csrf_field()}}<input type="date" name="datumOD" value="@php echo date('Y-09-01') @endphp" hidden> <input type="date" name="datumDO" value="@php echo date('Y-09-30') @endphp" hidden><input type="submit" value="Septembar" type="button" class="btn dugme btn-primary"><input type="number" name="filter" value="1" hidden></form>
			</div>
			<div class="col-md-2 mt-2 col-6">
   			<form action="/hosting/show" method="post" >{{csrf_field()}}<input type="date" name="datumOD" value="@php echo date('Y-10-01') @endphp" hidden> <input type="date" name="datumDO" value="@php echo date('Y-10-31') @endphp" hidden><input type="submit" value="Oktobar" type="button" class="btn dugme btn-primary"><input type="number" name="filter" value="1" hidden></form>
			</div>
			<div class="col-md-2 mt-2 col-6">
			<form action="/hosting/show" method="post" >{{csrf_field()}}<input type="date" name="datumOD" value="@php echo date('Y-11-01') @endphp" hidden> <input type="date" name="datumDO" value="@php echo date('Y-11-30') @endphp" hidden><input type="submit" value="Novembar" type="button" class="btn dugme btn-primary"><input type="number" name="filter" value="1" hidden></form>
			</div>
			<div class="col-md-2 mt-2 col-6">  
			<form action="/hosting/show" method="post" >{{csrf_field()}}<input type="date" name="datumOD" value="@php echo date('Y-12-01') @endphp" hidden> <input type="date" name="datumDO" value="@php echo date('Y-12-31') @endphp" hidden><input type="submit" value="Decembar" type="button" class="btn dugme btn-primary"><input type="number" name="filter" value="1" hidden></form>
			</div>
			</div>

@endsection