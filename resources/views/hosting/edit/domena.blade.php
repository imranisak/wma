@extends('master-hosting')
@section ('head')
    <title>Uredi domenu</title>
@include('partials.head-asset')
@endsection

@section ('content')
<h1>Dodaj domenu</h1>
<form method="post" action="/hosting/domena/edit/{{$domena->id}}">
	{{csrf_field()}}
	<input type="text" name="domena" value="{{$domena->domena}}">
	<input type="submit">
</form>

@endsection