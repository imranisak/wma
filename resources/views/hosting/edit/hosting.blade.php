@extends('master-hosting')
@section ('head')
    <title>Uredi hosting</title>
@include('partials.head-asset')
@endsection

@section ('content')
<h1>Uredi hosting</h1>
<form method="post" action="/hosting/edit/{{$hosting->id}}" class="form-horizontal" role="form">
	{{csrf_field()}}
	<div class="form-group">
		<div class="col-lg-3">
			Naziv domene:*<input type="text" name="naziv_domene" value="{{$hosting->naziv_domene}}" class="form-control" required>
		</div>
	</div>

	Domena:*
	<div class="form-group">
		<div class="col-lg-3">
			<select name="domena_id" class="selectpicker show-tick form-control" required>
				<option disabled >Izaberi domenu</option>
				@foreach($domena as $data)
				<option value="{{$data->id}}" <?php if ($hosting->domena_id==$data->id) echo 'selected="true"'; ?> >{{$data->domena}}</option>
				@endforeach
			</select>	
		</div>
	</div>
	Server:*
	<div class="form-group">
		<div class="col-lg-3">
			<select name="server_id" class="selectpicker show-tick form-control" required>
				<option disabled >Izaberi server</option>
				@foreach($server as $data)
				<option  value="{{$data->id}}" <?php if ($hosting->server_id==$data->id) echo 'selected="true"'; ?>>{{$data->server}}</option>
				@endforeach
			</select>
		</div>
	</div>

	Paket:*
	<div class="form-group">
		<div class="col-lg-3">	
			<select name="paket_id" class="selectpicker show-tick form-control" required>
						<option disabled >Izaberi paket</option>
						@foreach($paket as $data)
						<option  value="{{$data->id}}" <?php if ($hosting->paket_id==$data->id) echo 'selected="true"'; ?>>{{$data->paket}}</option>
						@endforeach
			</select>
		</div>
	</div>

	Ime i prezime:
	<div class="form-group">
		<div class="col-lg-3"> 
			<input type="text" name="kontakt" value="{{$hosting->kontakt}}" class="form-control">
		</div>
	</div>

	E-mail: 
	<div class="form-group">
		<div class="col-lg-3">
			<input type="email" name="mail" value="{{$hosting->mail}}"  class="form-control">
		</div>
	</div>

	Broj telefona:
	<div class="form-group">
		<div class="col-lg-3">
			<input type="text" name="telefon" value="{{$hosting->telefon}}" class="form-control">
		</div>
	</div>

	Datum obnove:
	<div class="form-group">
		<div class="col-lg-3">	
			<input type="date" name="datum_obnove" value="{{$hosting->datum_obnove}}" class="form-control">
		</div>
	</div>

	Cijena:
	<div class="form-group">
		<div class="col-lg-3">	
			<input type="number" name="cijena" step="0.01" value="{{$hosting->cijena}}" class="form-control">
		</div>
	</div>

	Napomena:
	<div class="form-group">
		<div class="col-lg-3">	
			<input type="text" name="napomena" value="{{$hosting->napomena}}" class="form-control">
		</div>
	</div>

	usluga:
	<div class="form-group">
		<div class="col-lg-3">	
			<input type="text" name="usluga" value="{{$hosting->usluga}}" class="form-control">
		</div>
	</div>


    <div class="form-check">
	  <input class="form-check-input" type="checkbox" value="1" id="statusPlacanja" name="statusPlacanja" <?php if($hosting->statusPlacanja==1) echo "checked='true'"; ?>>
	  	<label class="form-check-label" for="statusPlacanja">
			Plaćeno
			</label>
	</div>    

	<div class="form-check">
	  <input class="form-check-input" type="checkbox" value="1" id="suspend" name="suspend" <?php if($hosting->suspend==1) echo "checked='true'"; ?>>
	  	<label class="form-check-label" for="suspend">
			Suspend
			</label>
	</div>


	<input type="submit" value="Uredi" class="btn btn-primary">
</form>

@endsection