@extends('master-hosting')
@section ('head')
    <title>Uredi server</title>
@include('partials.head-asset')
@endsection

@section ('content')
<h1>Uredi server</h1>
<form method="post" action="/hosting/server/edit/{{$server->id}}">
	{{csrf_field()}}
	<input type="text" name="server" value="{{$server->server}}">
	<input type="submit">
</form>

@endsection