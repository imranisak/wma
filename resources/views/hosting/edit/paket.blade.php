@extends('master-hosting')
@section ('head')
    <title>Uredi paket</title>
@include('partials.head-asset')
@endsection

@section ('content')
<h1>Uredi paket</h1>
<form method="post" action="/hosting/paket/edit/{{$paket->id}}" class="form-horizontal" role="form">
	{{csrf_field()}}
	<input type="text" name="paket" value="{{$paket->paket}}">
	<input type="submit">
</form>

@endsection