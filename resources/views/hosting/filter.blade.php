<form method="post" action="/hosting/show">
{{csrf_field()}}
	<div class="form-row">
		<div class="col-lg-1">
			<label>Domena</label>
			<select name="domenafilter" class="selectpicker show-thin form-control">
				<option value="0"@if($filter==1) @if($domenafilter==0) @endif selected @endif>Sve</option>
				@foreach($domena as $data)
				<option value="{{$data->id}}"@if($filter==1) @if($domenafilter==$data->id) selected @endif @endif >{{$data->domena}}</option>
				@endforeach
			</select>	
		</div>

		<div class="col-lg-2">
			<label>Server</label>
			<select name="serverfilter" class="selectpicker show-thin form-control">
				<option value="0"@if($filter==1) @if($serverfilter==0) selected @endif @endif >Sve</option>
				@foreach($server as $data)
				<option value="{{$data->id}}"@if($filter==1) @if($serverfilter==$data->id) selected @endif @endif>{{$data->server}}</option>
				@endforeach
			</select>	
		</div>
		
		<div class="col-lg-2">
			<label>Paket</label>
			<select name="paketfilter" class="selectpicker show-thin form-control">
				<option value="0"@if($filter==1) @if($paketfilter==0) selected @endif @endif>Sve</option>
				@foreach($paket as $data)
				<option value="{{$data->id}}"@if($filter==1) @if($paketfilter==$data->id) selected @endif @endif>{{$data->paket}}</option>
				@endforeach
			</select>	
		</div>

		<div class="col-lg-1">
			<label>Prikaži</label>
			<select name="prikaz" class="selectpicker show-thin form-control">
			<option value="0"  @if($filter==1) @if($prikaz==0) selected @endif @endif>Sve</option>
			<option value="10" @if($filter==1) @if($prikaz==10) selected @endif @endif>10</option>
			<option value="20" @if($filter==1) @if($prikaz==20) selected @endif @endif>20</option>
			<option value="50" @if($filter==1) @if($prikaz==50) selected @endif @endif>50</option>
			</select>	
		</div>

		<div class="col-lg-2">
			<label>Od</label>
			<input type="date" name="datumOD" class="form-control" @if($filter==1) value="{{$datumOD}}" @endif>	
		</div>

		<div class="col-lg-2">
			<label>Do</label>
			<input type="date" name="datumDO" class="form-control" @if($filter==1) value="{{$datumDO}}" @endif>	
		</div>
	</div>	

	<input type="number" name="filter" hidden value="1">

	<input type="submit" type="button" value="Filtriraj" class="btn btn-primary mt-2 mb-2">	
	
	@if($filter==1)<a href="/hosting/show"><button type="button" class="btn btn-danger">Resetuj filter</button></a>	@endif<br>
	<input type="text" class="form-control col-md-3" id="domenaTrazilica" placeholder="Trazi u tabeli">
</form>
