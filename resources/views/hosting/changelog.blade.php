@extends('master-hosting')
@section('head')
	<title>Hosting Changelog</title>
	@include('partials.head-asset')
@endsection

@section('content')
	<h3>Web Media Hosting <sub>By Imran Isak</sub></h3>
	<h3>Hosting Changelog</h3>

	<dl>
		<dt> v0.1
			<li>Početna verzija</li>
			<li>Sve osim filtera po datumu osposobljeno</li>
		</dt>		
		<br>
		<dt> v0.2
			<li>Osposobljen datum po filteru</li>
			<li>Dodana upozorenja i sl.</li>
		</dt>		
		<br>
		<dt> v1.0
			<li>Sposobnost brisanja i uređivanja domene, hostinga i paketa</li>
			<li>Dodani butttoni na home, za mjesece tekuće godine</li>
			<li>Opcija detaljnog pregleda hostinga, klikom na naziv domene</li>
			<li>Ako je napomena preduka, prikaže samo "...", ujesto da poremeti tabelu</li>
			<li>Puna verzija</li>
		</dt>
		

	</dl>

@endsection