@extends('master-hosting')
@section('head')
<title>Detalji za domenu {{$hosting->naziv_domene}}</title>
@include('partials.head-asset')
<style>
	h3, h4 {display: inline;}
	hr{width: 80%;margin-left: 0px;}
</style>
@endsection


@section('content')
<h3>Naziv domene: </h3> <h4>{{$hosting->naziv_domene}}</h4><br><hr>
<h3>Domena: </h3><h4>{{$hosting->domena->domena}}</h4><br><hr>
<h3>Server: </h3><h4>{{$hosting->server->server}}</h4><br><hr>
<h3>Ime i prezime: </h3><h4>{{$hosting->kontakt}}</h4><br><hr>
<h3>E-mail: </h3><h4>{{$hosting->mail}}</h4><br><hr>
<h3>Broj telefona: </h3><h4>{{$hosting->telefon}}</h4><br><hr>
<h3>Datum obnove: </h3><h4>{{$hosting->datum_obnove}}</h4><br><hr>
<h3>Cijena: </h3><h4>{{$hosting->cijena}}</h4><br><hr>
<h3>Napomena: </h3><textarea readonly class="form-control" rows="3">{{$hosting->napomena}}</textarea><br>
<a href="/hosting/edit/{{$hosting->id}}"><button class="btn btn-primary">Uredi</button></a>
@endsection