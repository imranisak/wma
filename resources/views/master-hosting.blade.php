<!DOCTYPE html>
<html>
<head>
	@include('partials.head-asset')
	@yield('head')
</head>
<body>
	@include('partials.nav-hosting')
<div class="container">
	<div class="col-md-12 mt-3">
				
				@include('partials.alerts')
				@yield('content')
	</div>
</div>

</body>
</html>