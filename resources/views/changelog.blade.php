@extends('master')
@section('head')
	<title>Changelog</title>
	@include('partials.head-asset')
@endsection

@section('content')

<h3>Web Media Finansije <sub>By Imran Isak</sub></h3>
	<h3>Changelog</h3>

	<dl>
		<dt> v0.1
			<li>Početna verzija</li>
			<li>Dodan UI i navbar</li>
		</dt>
		<br>
		<dt> v0.2
			<li>Dodane opcije da se mogu unjeti načini plaćanja i sektori</li>
			<li>Dodano TRENUTNO stanje računa</li>
		</dt>
		<br>	
		<dt> v0.3
			<li>Dodan FILTER po sektoru, načinu plaćanja, sektoru i ulazu/izlazu</li>
			<li>Dodan LOGIN</li>
			<li>Dodana mogućnost uređenje i brisanja prometa</li>
			<li>Utegnuta tabela</li>
			<li>Utegnuta buttoni</li>
			<li>UI generalno prerađen</li>
		</dt>
		<br>
		<dt> v0.3.1
			<li>Dodan ovaj changelog</li>
		</dt>
		<br>
		<dt> v0.4
			<li>Dodan filter po datumu</li>
		</dt>
		<br>	
		<dt> v0.5
			<li>Obojen red za iznos</li>
			<li>Dodana opcija "prikaži samo"</li>
			<li>Dodana ukupna suma za SAMO filter</li>
			<li>Mogućnost unosa decimale (mora biti tačka, ne zarez)</li>
		</dt>		
		<br>
		<dt> v0.5.1
			<li>Uređeno dodavanje iznosa u dodavanju prometa, da se može SAMO tačka unjeti..da ne bude konflikta</li>
		</dt>		
		<dt> v0.6
			<li>UI opet utegnut</li>
		</dt>		
		<br>
		<dt> v0.6.1
			<li>Zadani prikaz na "Prikaži stanje" je sada 10 zadnjih transakcija</li>
			<li>Ako se ne unosu oba datuma, dobije se error, ne crash!</li>
		</dt>		
		<br>
		<dt> v0.6.2
			<li>Dotjeran filter po datumu</li>
		</dt>	
		<br>
		<dt> v0.7
			<li>Dodana opcija da se printa filtrirana tabela.</li>
		</dt><br>
		<dt> v1.0
			<li>Bug fixes</li>
			<li>Dodano stanje za tekući mjesec</li>
			<li>Puna verzija</li>
		</dt><br>		
		<dt> v1.1
			<li>Dodani dugovi</li>
		</dt><br>
	</dl>

@endsection