@extends('master-fiksni')
@section('head')
	<title>Dodaj fiksni trošak</title>
	@include('partials.head-asset')
@endsection

@section ('content')
	<h1>Dodaj fiksni trošak</h1>
	<form method="POST" action="/fiksni/add" class="form">
		{{ csrf_field() }}
		<input type="text" name="trosak" required> <br> <br>
		<input type="submit" class="btn btn-success">
	</form>
@endsection