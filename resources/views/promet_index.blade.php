@extends('master')
@section('head')
	<title>Home</title>
	@include('partials.head-asset')
@endsection

@section('content')
	<!--<a href="/"><button type="button" class="btn btn-success">Dodaj ulaz</button>  </a><span><a href="/"><button type="button" class="btn btn-danger">Dodaj izlaz</button></a></span>
	<br>
	<br>
	<a href="/sektor/add"><button type="button" class="btn btn-primary">Dodaj Sektor</button>  </a><span><a href="/nacinplacanja/add"><button type="button" class="btn btn-primary">Dodaj Način plačanja</button></a></span>
	<hr>
	-->
	<h1>Welcome</h1>
	<h3>Zadnje tri transakcije:</h3>
	<br>

	@foreach($promet as $pr)

		Vrsta: @if($pr->ulaz_izlaz==1) Ulaz<br> @else Izlaz<br>  @endif
		Sektor: {{$pr->sektor->sektor}}<br>
		Način plaćanja: {{$pr->nacinplacanja->nacinplacanja}}<br>
		Iznos: {{$pr->iznos}}<br>
		Napomena: {{$pr->napomena}}<br>
		Datum unosa: {{$pr->created_at}}<br>
		<hr>

	@endforeach

@endsection