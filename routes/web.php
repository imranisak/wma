<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>['auth']], function(){
	Route::get('/','index@index');
	//HOSTING
	Route::get('/hosting','HostingController@index')->name('hosting');
	Route::get('/hosting/add','HostingController@create');
	Route::post('/hosting/add','HostingController@store');
	Route::get('/hosting/ucitaj','HostingController@ucitaj');

	Route::get('/hosting/edit/{id}','HostingController@edit');
	Route::post('/hosting/edit/{id}','HostingController@update');
	Route::get('/hosting/delete/{id}','HostingController@destroy');

	Route::get('/hosting/show','HostingController@show');
	Route::post('/hosting/show','HostingController@filter');
	Route::get('/hosting/details/{id}','HostingController@details');

	Route::get('/hosting/server/add','ServerController@create')->name('dodajServer');
	Route::post('/hosting/server/add','ServerController@store');
	Route::get('/hosting/server/edit/{id}','ServerController@edit');
	Route::post('/hosting/server/edit/{id}','ServerController@update');
	Route::get('/hosting/server/delete/{id}','ServerController@destroy');

	Route::get('/hosting/paket/add','PaketController@create')->name('dodajPaket');
	Route::post('/hosting/paket/add','PaketController@store');
	Route::get('/hosting/paket/edit/{id}','PaketController@edit');
	Route::post('/hosting/paket/edit/{id}','PaketController@update');
	Route::get('/hosting/paket/delete/{id}','PaketController@destroy');

	Route::get('/hosting/domena/add','DomenaController@create')->name('dodajDomenu');
	Route::post('/hosting/domena/add','DomenaController@store');
	Route::get('/hosting/domena/edit/{id}','DomenaController@edit');
	Route::post('/hosting/domena/edit/{id}','DomenaController@update');
	Route::get('/hosting/domena/delete/{id}','DomenaController@destroy');

	Route::post('/poruke/dodaj','PorukeController@store');
	Route::get('/poruke/ucitaj','PorukeController@index');
	Route::get('/poruke/obrisi/{id}','PorukeController@destroy');

	Route::get('/statusPlacanja','statusPlacanja@index');

	Route::get('/hosting/changelog','HostingController@changelog');

	//FINANSIJE
	Route::get('/finansije/index','PrometController@index');
	Route::get('/sektor/add','SektorController@create');
	Route::post('/sektor/add','SektorController@store');
	Route::get('/nacinplacanja/add','NacinPlacanjaController@create');
	Route::post('/nacinplacanja/add','NacinPlacanjaController@store');

	Route::get('/promet/add','PrometController@create');
	Route::post('/promet/add','PrometController@store');
	Route::get('/stanje','PrometController@show');
	Route::post('/stanje','PrometController@filter');

	Route::get('/promet/edit/{id}' ,'PrometController@edit');
	Route::post('/promet/edit/{id}' ,'PrometController@update');
	Route::get('/promet/delete/{id}' ,'PrometController@destroy');
	Route::get('/home', 'PrometController@index')->name('home');

	Route::post('/dug/add','DugController@store');


	//FIKSNI TROŠKOVI
	Route::get('/fiksni','FiksniController@index');
	Route::get('/fiksni/add','TroskoviController@create');
	Route::post('/fiksni/add','TroskoviController@store');

	Route::get('/changelog','PrometController@changelog');

	//MAIL TEST
	Route::get('/mail','MailController@sendMail');
});

Route::get('/login','Auth\LoginController@showLoginForm')->name('login');
Route::post('/login','Auth\LoginController@login');
Route::get('/logout','Auth\LoginController@logout');

//Route::get('/register','Auth\RegisterController@create');

//Auth::routes();
